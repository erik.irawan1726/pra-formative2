
public class MainVehicle{

    public static void main(String[] args) {
        // write your code here
        Sepeda s = new Sepeda();
        Motor m = new Motor();
        System.out.println("Sepeda:");
        s.tenaga();
        s.cemar();
        s.harga();
        s.maksimum();
        s.roda();
        System.out.println("Motor:");
        m.tenaga();
        m.cemar();
        m.harga();
        m.maksimum();
        m.roda();

    }
}
