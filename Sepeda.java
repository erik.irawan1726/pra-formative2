public class Sepeda extends Vehicle {
    @Override
    public void tenaga() {
        System.out.println("Bahan bakar yaitu karbohidrat");
    }
    public void cemar() {
        System.out.println("Sepeda tidak mencemari lingkungan");
    }
    public void harga() {
        System.out.println("Harga Sepeda Balap yaitu 2 sampai 200 juta");
    }
    public void maksimum() {
        System.out.println("Kecepatan maksimum sepeda yaitu 60km/jam");
    }
}