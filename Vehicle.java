public abstract class Vehicle {
    public abstract void tenaga();
    public abstract void cemar();
    public abstract void harga();
    public abstract void maksimum();

    public void roda(){
        System.out.println("Kendaraan ini menggunakan roda-2");
        System.out.println("...................................");
    }

//    public abstract void harga();
}