public class Motor extends Vehicle {
    @Override
    public void tenaga() {
        System.out.println("Bahan bakar yaitu Bensin");
    }
    public void cemar() {
        System.out.println("Motor dapat mencemari lingkungan");
    }
    public void harga() {
        System.out.println("Harga Motor Balap yaitu 42 sampai 49 miliar");
    }
    public void maksimum() {
        System.out.println("Kecepatan maksimum motor yaitu 360km/jam");
    }

}